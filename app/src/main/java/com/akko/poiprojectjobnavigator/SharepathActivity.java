package com.akko.poiprojectjobnavigator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.akko.poiprojectjobnavigator.UtilPreference.SharedPreferencesUtil;
import com.akko.poiprojectjobnavigator.NitroLibrary.ui.transfer.TransferActivity;
import com.akko.poiprojectjobnavigator.Wifilibrary.WifiActivity;

public class SharepathActivity extends AppCompatActivity {

    private Button toMain,letshare,letwifi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sharepath);
        letshare = (Button) findViewById(R.id.letshare);
        toMain = (Button) findViewById(R.id.back);
        letwifi = (Button) findViewById(R.id.letwifi);

        if(SharedPreferencesUtil.getValue(SharepathActivity.this,"LANG").equals("en")) {
            toMain.setText("ฺBACK");
            letshare.setText("SHARE");
            letwifi.setText("WIFI");
        }else{
            toMain.setText("ย้อนกลับ");
            letshare.setText("แชร์");
            letwifi.setText("เชื่อมต่อไร้สาย");
        }

        letwifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SharepathActivity.this, WifiActivity.class);
                startActivity(intent);
                finish();
            }
        });

        letshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SharepathActivity.this, TransferActivity.class);
                startActivity(intent);
                finish();
            }
        });
        toMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SharepathActivity.this, MainActivity.class);
                intent.putExtra("finished", "");
                intent.putExtra("from", SettingActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SharepathActivity.this, MainActivity.class);
        intent.putExtra("finished", "");
        intent.putExtra("from", SettingActivity.class);
        startActivity(intent);
        finish();
    }
}
