package com.akko.poiprojectjobnavigator;

import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//import java.security.Permissions;
import java.util.Locale;

// This class is first menu
public class MainActivity extends AppCompatActivity {
    private Button setting, nav, rec, del, share;
    private boolean mShowPermissionSlide;
    static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nav = (Button) findViewById(R.id.navBtn);
        rec = (Button) findViewById(R.id.recBtn);
        setting = (Button) findViewById(R.id.settingฺBtn);
        del = (Button) findViewById(R.id.delBtn);
        share = (Button) findViewById(R.id.shareBtn);

//        Old permission
//        mShowPermissionSlide = !Permissions.haveStoragePermission(this);
//        if (mShowPermissionSlide) {
//            Permissions.requestStoragePermission(this);
//        }
//        Locale.getDefault().getDisplayLanguage();

//        SharedPreferencesUtil.addValue(MainActivity.this, "LANG", Locale.getDefault().getDisplayLanguage());
//        if(SharedPreferencesUtil.getValue(MainActivity.this,"LANG").equals("ENGLISH")){


//        Auto detect language
        if (Locale.getDefault().getDisplayLanguage().equals("ไทย")) {
//            share.setText("SHARE PATH");
//            share.setText(Locale.getDefault().getDisplayLanguage());
            share.setText("แชร์เส้นทาง");
            setting.setText("ตั้งค่า");
            del.setText("ลบเส้นทาง");
            nav.setText("เริ่มนำทาง");
            rec.setText("บันทึกเส้นทาง");

        } else {
//            share.setText("แชร์เส้นทาง");
//            share.setText(Locale.getDefault().getDisplayLanguage());
            share.setText("SHARE PATH");
            setting.setText("SETTING");
            del.setText("DELETE PATH");
            nav.setText("NAVIGATION");
            rec.setText("RECORD PATH");
        }


//        Set button menu
        nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SelectDesActivity.class);
                startActivity(intent);
                finish();
            }
        });

        rec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RecordActivity.class);
                startActivity(intent);
                finish();
            }
        });

        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intent);
                finish();
            }
        });

        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DeleteActivity.class);
                startActivity(intent);
                finish();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, SharepathActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
