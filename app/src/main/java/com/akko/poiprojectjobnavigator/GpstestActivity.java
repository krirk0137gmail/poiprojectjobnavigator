package com.akko.poiprojectjobnavigator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

//  this class use to test gps
public class GpstestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gpstest);
    }


    //    Set default back button
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(GpstestActivity.this, MainActivity.class);
        intent.putExtra("finished", "");
        intent.putExtra("from", GpstestActivity.class);
        startActivity(intent);
        finish();
    }


}
