package com.akko.poiprojectjobnavigator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.akko.poiprojectjobnavigator.UtilPreference.SharedPreferencesUtil;

// This class use for Record path to collect it
public class RecordActivity extends AppCompatActivity {

    private Button ptt;
    private Button txtInput;
    private Button startRec;
    private Button toMain;
    private EditText editTxtInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        editTxtInput = (EditText) findViewById(R.id.editTxtInput);
        ptt = (Button) findViewById(R.id.ptt);
        txtInput = (Button) findViewById(R.id.txtInput);
        toMain = (Button) findViewById(R.id.toMainBtn);
        startRec = (Button) findViewById(R.id.startRecTxtInput);

//        Auto detect language
        if (SharedPreferencesUtil.getValue(RecordActivity.this, "LANG").equals("en")) {
            toMain.setText("ฺBACK");
            ptt.setText("VOICE ACTION");
            txtInput.setText("KEYBOARD ACTION");
        } else {
            toMain.setText("ย้อนกลับ");
            ptt.setText("สั่งการด้วยเสียง");
            txtInput.setText("สั่งการด้วยคีย์บอร์ด");
        }

//        set button to back
        toMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RecordActivity.this, MainActivity.class);
                intent.putExtra("finished", "");
                intent.putExtra("from", RecordActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    //    set default back button
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RecordActivity.this, MainActivity.class);
        intent.putExtra("finished", "");
        intent.putExtra("from", RecordActivity.class);
        startActivity(intent);
        finish();
    }


}
