package com.akko.poiprojectjobnavigator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.akko.poiprojectjobnavigator.UtilPreference.SharedPreferencesUtil;

// This path use to select path
public class SelectDesActivity extends AppCompatActivity {
    private Button ptt, gps;
    private Button toMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_des);
        gps = (Button) findViewById(R.id.gps);
        toMain = (Button) findViewById(R.id.back);


//        Auto detect language
        if (SharedPreferencesUtil.getValue(SelectDesActivity.this, "LANG").equals("en")) {
            gps.setText("GPSTEST");
            toMain.setText("ฺBACK");
        } else {
            gps.setText("ทดสอบระบบ");
            toMain.setText("ย้อนกลับ");
        }

//         Set button
        gps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SelectDesActivity.this, GpstestActivity.class);
                startActivity(intent);
                finish();
            }
        });

        toMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SelectDesActivity.this, MainActivity.class);
                intent.putExtra("finished", "");
                intent.putExtra("from", SettingActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    //     Set default back  button
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SelectDesActivity.this, MainActivity.class);
        intent.putExtra("finished", "");
        intent.putExtra("from", SelectDesActivity.class);
        startActivity(intent);
        finish();
    }

}
