package com.akko.poiprojectjobnavigator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.akko.poiprojectjobnavigator.UtilPreference.SharedPreferencesUtil;


// this class use to delete path
public class DeleteActivity extends AppCompatActivity {

    private Button toMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);
        toMain = (Button) findViewById(R.id.backBtn);

//        Use to set auto detect language
        if (SharedPreferencesUtil.getValue(DeleteActivity.this, "LANG").equals("en")) {
            toMain.setText("ฺBACK");
        } else {
            toMain.setText("ย้อนกลับ");
        }

//        set button
        toMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DeleteActivity.this, MainActivity.class);
                intent.putExtra("finished", "");
                intent.putExtra("from", DeleteActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    //     set default back button on phone
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(DeleteActivity.this, MainActivity.class);
        intent.putExtra("finished", "");
        intent.putExtra("from", DeleteActivity.class);
        startActivity(intent);
        finish();
    }

}
