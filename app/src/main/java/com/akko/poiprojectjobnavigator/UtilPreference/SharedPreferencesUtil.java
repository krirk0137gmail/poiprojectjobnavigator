package com.akko.poiprojectjobnavigator.UtilPreference;

import android.content.Context;
import android.content.SharedPreferences;
// Keep Memory     ( same Sql )

/**
 * Created by admin on 11/24/17.
 */
//  This Class use to memory the value  and bring value to use
public class SharedPreferencesUtil {
    private static final String KEY_NOT_ENCODE = "KEY_NOT_ENCODE";
    private static final String KEY_ENCODE = "KEY_ENCODE";


    public static void addValue(Context context, String key, String value) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NOT_ENCODE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void addValue(Context context, String key, int value) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NOT_ENCODE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(key, value);
        editor.commit();
    }
    public static void addBoolean(Context context, String key, boolean value) {
        SharedPreferences pref = context.getSharedPreferences(KEY_NOT_ENCODE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void addEncodeValue(Context context, String key, String value) {
        SharedPreferences pref = context.getSharedPreferences(KEY_ENCODE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        //       TODO Encode Value Before Push Into SharedPreFerences
        editor.putString(key, value);
        editor.commit();
    }

    public static String getValue(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_NOT_ENCODE, Context.MODE_PRIVATE);
        String couponNew = sharedPreferences.getString(key, "");
        if (couponNew == null) {
            sharedPreferences = context.getSharedPreferences(KEY_ENCODE, Context.MODE_PRIVATE);
            couponNew = sharedPreferences.getString(key, "");
            //       TODO Decode Value Before Push Into SharedPreFerences

            if (couponNew == null) {
                return null;
            }

        }
        return couponNew;
    }
    public static String getValueAccept(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_NOT_ENCODE, Context.MODE_PRIVATE);
        String couponNew = sharedPreferences.getString(key, "wait");
        if (couponNew == null) {
            sharedPreferences = context.getSharedPreferences(KEY_ENCODE, Context.MODE_PRIVATE);
            couponNew = sharedPreferences.getString(key, "wait");
            //       TODO Decode Value Before Push Into SharedPreFerences

            if (couponNew == null) {
                return "wait";
            }

        }
        return couponNew;
    }

    public static int getValueInt(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_NOT_ENCODE, Context.MODE_PRIVATE);
        int couponNew = sharedPreferences.getInt(key, 0xFF06b390);
        if (couponNew == 0xFF06b390) {
            sharedPreferences = context.getSharedPreferences(KEY_ENCODE, Context.MODE_PRIVATE);
            couponNew = sharedPreferences.getInt(key, 0xFF06b390);
            //       TODO Decode Value Before Push Into SharedPreFerences

            if (couponNew == 0xFF06b390) {
                return 0xFF06b390;
            }

        }
        return couponNew;
    }


    public static boolean getBoolean(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_NOT_ENCODE, Context.MODE_PRIVATE);
        boolean couponNew = sharedPreferences.getBoolean(key, false);
        return couponNew;
    }
    public static String getValueText(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_NOT_ENCODE, Context.MODE_PRIVATE);
        int couponNew = sharedPreferences.getInt(key, 0xFF06b390);
        if (couponNew == 0xFF06b390) {
            sharedPreferences = context.getSharedPreferences(KEY_ENCODE, Context.MODE_PRIVATE);
            couponNew = sharedPreferences.getInt(key, 0xFF06b390);
            //       TODO Decode Value Before Push Into SharedPreFerences
//        EVERY PLUS THEME  JUST + NUMBER IN SHAREPREFERENCE   EX.1 2 3   plus 4  = 2 3 4 5   ok
            if (couponNew == 0xFF06b390) {
                return "Now GREEN";
            }else if (couponNew == 2131755218) {
                return "Now GREEN";
            }else if(couponNew == 2131755219){
                return "Now SKY";
            }else if(couponNew == 2131755220){
                return "Now PINK";
            }else if(couponNew == 2131755221){
                return "Now RED";
            }else if(couponNew == 2131755222){
                return "Now RED2";
            }else if(couponNew == 2131755223){
                return "Now ORANGE";
            }else{
                return ""+couponNew+"xx";
            }

        }else if (couponNew == 2131755218) {
            return "Now GREEN";
        }else if(couponNew == 2131755219){
            return "Now SKY";
        }else if(couponNew == 2131755220){
            return "Now PINK";
        }else if(couponNew == 2131755221){
            return "Now RED";
        }else if(couponNew == 2131755222){
            return "Now RED2";
        }else if(couponNew == 2131755223){
            return "Now ORANGE";
        }else{
            return ""+couponNew+"xx";
        }
    }

}
