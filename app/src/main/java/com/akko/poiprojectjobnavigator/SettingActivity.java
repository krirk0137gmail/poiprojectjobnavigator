package com.akko.poiprojectjobnavigator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.akko.poiprojectjobnavigator.UtilPreference.SharedPreferencesUtil;

//  This class use for setting menu
public class SettingActivity extends AppCompatActivity {

    private ListView settingList;
    private Button toMain;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

//        settingList = (ListView) findViewById(R.id.settingList);
        toMain = (Button) findViewById(R.id.back);
//        String about = getString(R.string.about);
//        initAdapter();


//        Auto detect language
        if (SharedPreferencesUtil.getValue(SettingActivity.this, "LANG").equals("en")) {
            toMain.setText("ฺBACK");
        } else {
            toMain.setText("ย้อนกลับ");
        }

//         Set button
        toMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SettingActivity.this, MainActivity.class);
                intent.putExtra("finished", "");
                intent.putExtra("from", SettingActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    //  Set default back button
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SettingActivity.this, MainActivity.class);
        intent.putExtra("finished", "");
        intent.putExtra("from", SettingActivity.class);
        startActivity(intent);
        finish();
    }


}
